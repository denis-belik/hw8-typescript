export interface ModalContent {
    title: string;
    bodyElement: HTMLElement;
}