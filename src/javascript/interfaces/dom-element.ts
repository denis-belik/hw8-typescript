export interface DomElement {
    tagName: string;
    className?: string;
    attributes?: { [key: string]: string; };
}