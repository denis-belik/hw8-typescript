import { Fighter } from "./fighter";

export interface FighterDetails extends Fighter {
    health: number;
    attack: number;
    defense: number;
}