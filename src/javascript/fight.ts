import { FighterDetails } from "./interfaces/fighter-details";
import { getRandomNumber } from "./helpers/randomHelper";
import { Fighter } from "./interfaces/fighter";

export function fight(firstFighter: FighterDetails, secondFighter: FighterDetails): Fighter {
  let { health: firstFighterActualHealth }= firstFighter;
  let { health: secondFighterActualHealth } = secondFighter;

  while(secondFighterActualHealth > 0) {
    firstFighterActualHealth -= getDamage(secondFighter, firstFighter);

    if(firstFighterActualHealth <= 0) {
      return secondFighter;
    }
    secondFighterActualHealth -= getDamage(firstFighter, secondFighter);
  }

  return firstFighter;
}

export function getDamage(attacker: FighterDetails, enemy: FighterDetails): number {
  const hit: number = getHitPower(attacker);
  const block: number = getBlockPower(enemy);

  return block > hit ? 0 : hit - block;
}

export function getHitPower(fighter: FighterDetails): number {
  return fighter.attack * getRandomNumber(1, 2);
}

export function getBlockPower(fighter: FighterDetails): number {
  return fighter.defense * getRandomNumber(1, 2);
}
