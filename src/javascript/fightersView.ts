import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { FighterDetails } from './interfaces/fighter-details';
import { getFighterDetails } from './services/fightersService';
import { Fighter } from './interfaces/fighter';

export function createFighters(fighters: Fighter[]): HTMLDivElement {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements: HTMLDivElement[] = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer: HTMLDivElement = createElement<HTMLDivElement>({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache: Map<string, FighterDetails> = new Map();

async function showFighterDetails(event: Event, fighter: Fighter): Promise<void> {
  const fullInfo: FighterDetails = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string): Promise<FighterDetails> {
  // get fighter form fightersDetailsCache or use getFighterDetails function
  return await getFighterDetails(fighterId);
}

function createFightersSelector(): (event: Event, fighter: Fighter) => Promise<void> {
  const selectedFighters: Map<string, FighterDetails> = new Map();

  return async function selectFighterForBattle(event: Event, fighter: Fighter): Promise<void> {
    (<HTMLInputElement>event.target).setAttribute('disabled', '');
    const fullInfo: FighterDetails = await getFighterInfo(fighter._id);

    if ((<HTMLInputElement>event.target).checked) {
      selectedFighters.set(fighter._id, fullInfo);
      console.log('set');
    } else { 
      selectedFighters.delete(fighter._id);
      console.log('delete');
    }

    if (selectedFighters.size === 2) {
      const fightersArray: FighterDetails[] = Array.from(selectedFighters, ([id, info]) => info);
      const winner: Fighter = fight(<FighterDetails>fightersArray.pop(), <FighterDetails>fightersArray.pop());
      showWinnerModal(winner);
    }

    (<HTMLInputElement>event.target).removeAttribute('disabled');
  }
}