import { createElement } from './helpers/domHelper'; 
import { Fighter } from './interfaces/fighter';

function createFighter(fighter: Fighter, 
                              handleClick: (e: Event, f: Fighter) => void, 
                              selectFighter: (e: Event, f: Fighter) => void)
                              : HTMLDivElement {
  const { name, source } = fighter;
  const nameElement: HTMLSpanElement = createName(name);
  const imageElement: HTMLImageElement = createImage(source);
  const checkboxElement: HTMLLabelElement = createCheckbox(/*source*/);
  const fighterContainer: HTMLDivElement = createElement<HTMLDivElement>({ tagName: 'div', className: 'fighter' });
  
  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev: Event) => ev.stopPropagation();
  const onCheckboxClick = (ev: Event) => selectFighter(ev, fighter);
  const onFighterClick = (ev: Event) => handleClick(ev, fighter);

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick , false);

  return fighterContainer;
}

function createName(name: string): HTMLSpanElement {
  const nameElement: HTMLSpanElement = createElement<HTMLSpanElement>({ tagName: 'span', className: 'name' });
  nameElement.innerText = name;

  return nameElement;
}

function createImage(source: string): HTMLImageElement {
  const attributes = { src: source };
  const imgElement: HTMLImageElement = createElement<HTMLImageElement>({ tagName: 'img', className: 'fighter-image', attributes });

  return imgElement;
}

function createCheckbox(): HTMLLabelElement {
  const label: HTMLLabelElement = createElement<HTMLLabelElement>({ tagName: 'label', className: 'custom-checkbox' });
  const span: HTMLSpanElement = createElement<HTMLSpanElement>({ tagName: 'span', className: 'checkmark' });
  const attributes = { type: 'checkbox' };
  const checkboxElement: HTMLInputElement = createElement<HTMLInputElement>({ tagName: 'input', attributes });

  label.append(checkboxElement, span);
  return label;
}

export { createName }
export { createImage }
export { createFighter }