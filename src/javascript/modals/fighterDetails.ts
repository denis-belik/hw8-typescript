import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { FighterDetails } from '../interfaces/fighter-details';
import { fight } from '../fight';
import { createImage } from '../fighterView';

export function showFighterDetailsModal(fighter: FighterDetails): void {
  const title: string = 'Fighter info';
  const bodyElement: HTMLDivElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: FighterDetails): HTMLDivElement {
  const { name, health, attack, defense, source } = fighter;

  const fighterDetails: HTMLDivElement = createElement<HTMLDivElement>({ tagName: 'div', className: 'modal-body' });
  const nameElement: HTMLSpanElement = createElement<HTMLSpanElement>({ tagName: 'span', className: 'fighter-property' });
  const healthElement: HTMLSpanElement = createElement<HTMLSpanElement>({ tagName: 'span', className: 'fighter-property' });
  const attackElement: HTMLSpanElement = createElement<HTMLSpanElement>({ tagName: 'span', className: 'fighter-property' });
  const defenseElement: HTMLSpanElement = createElement<HTMLSpanElement>({ tagName: 'span', className: 'fighter-property' });
  const imageElement: HTMLImageElement = createImage(source);

  // show fighter name, attack, defense, health, image
  nameElement.innerText = name;
  healthElement.innerText = `Health: ${health}`;
  attackElement.innerText = `Attack: ${attack}`;
  defenseElement.innerText = `Defence: ${defense}`;
  
  fighterDetails.append(nameElement, healthElement, attackElement, defenseElement, imageElement);

  return fighterDetails;
}
