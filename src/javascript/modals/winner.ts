import { showModal } from "./modal";
import { Fighter } from "../interfaces/fighter";
import { createElement } from "../helpers/domHelper";
import { createImage, createName } from "../fighterView";

export  function showWinnerModal(fighter: Fighter): void {
  // show winner name and image
  const bodyElement: HTMLDivElement = createWinnerDetails(fighter);
  const title = 'Winner';
  showModal({ title, bodyElement });
}

function createWinnerDetails(fighter: Fighter): HTMLDivElement {
  const { name, source } = fighter;

  const fighterDetails: HTMLDivElement = <HTMLDivElement>createElement({ tagName: 'div', className: 'winner-modal-body' });
  const nameElement: HTMLSpanElement = createName(name);
  const imageElement: HTMLImageElement = <HTMLImageElement>createImage(source);

  nameElement.innerText = name;
  fighterDetails.append(nameElement, imageElement);

  return fighterDetails;
}