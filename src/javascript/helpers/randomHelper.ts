export function getRandomNumber(min: number, maxExclusive: number): number {
    return Math.random() * (maxExclusive - min) + min;
}